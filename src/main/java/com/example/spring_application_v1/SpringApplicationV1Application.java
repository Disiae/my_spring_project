package com.example.spring_application_v1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringApplicationV1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringApplicationV1Application.class, args);
    }

}
