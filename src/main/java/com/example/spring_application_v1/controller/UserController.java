package com.example.spring_application_v1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @GetMapping("/hello")
    public String hello (@RequestParam String name, int age)
    {
        return "Hello " + name + " " + age;
    }
    @GetMapping("/hi")
    public String hi()
        {return " Hello";}
    @GetMapping()
    public String orig (){
        return "WELCOME";
        }
    }
 